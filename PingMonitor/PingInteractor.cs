﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace PingMonitor
{
    public class PingInteractor : IDisposable
    {
        private Ping _netPing;

        public PingInteractor()
        {
            _netPing = new System.Net.NetworkInformation.Ping();
        }

        public async Task<Results> Send(string hostNameOrAddress, int timeoutInMilliseconds, byte[] buffer, PingOptions options, int delayAfterSuccessInMilliseconds)
        {
            var results = await _netPing.SendPingAsync(hostNameOrAddress, timeoutInMilliseconds, buffer, options);
            
            if(results.Status == IPStatus.Success)
            {
                Thread.Sleep(delayAfterSuccessInMilliseconds);
            }

            return new Results(DateTime.UtcNow, results.Address.ToString(), results.RoundtripTime, results.Status);
        }
        void IDisposable.Dispose()
        {
            _netPing.Dispose();
            _netPing = null;
        }
    }
}
