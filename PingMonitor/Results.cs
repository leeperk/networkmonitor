﻿using System;
using System.Net.NetworkInformation;

namespace PingMonitor
{
    public class Results
    {
        public DateTime TimeStamp { get; }
        public string Address { get; }
        public long RoundTripTime { get; }
        public IPStatus Status { get; }

        public Results(DateTime timeStamp, string address, long roundTripTime, IPStatus status)
        {
            TimeStamp = timeStamp;
            Address = address;
            RoundTripTime = roundTripTime;
            Status = status;
        }
    }
}
