﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace PingMonitor
{
    public class Program
    {
        private const string _addressToPing = "8.8.8.8";
        private static readonly string rootDirectory = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        private static readonly string dataFileName = Path.Combine(rootDirectory, "ping.csv");

        static async Task Main(string[] args)
        {
            try
            {
                using (var ping = new PingInteractor())
                {
                    var pingOptions = new PingOptions() { DontFragment = true };
                    var data = new string('a', 32);
                    var buffer = Encoding.ASCII.GetBytes(data);
                    var timeout = 1000;
                    await ping.Send(_addressToPing, timeout, buffer, pingOptions, 1000);
                    await ping.Send(_addressToPing, timeout, buffer, pingOptions, 1000);
                    var results = await ping.Send(_addressToPing, timeout, buffer, pingOptions, 0);
                    DisplayPingResult(results);
                    WriteResultsToDataFile(results);
                }
            }
            catch (Exception)
            {
                WriteResultsToDataFile(new Results(DateTime.UtcNow, _addressToPing, -1, IPStatus.Unknown));
            }
        }

        private static void DisplayPingResult(Results results)
        {
            Console.WriteLine($"Status: {results.Status}");
            if (results.Status == IPStatus.Success)
            {
                Console.WriteLine("Address: {0}", results.Address);
                Console.WriteLine("RoundTrip time: {0}", results.RoundTripTime);
            }
            Console.WriteLine();
        }

        private static void WriteResultsToDataFile(Results results)
        {
            var csvConfiguration = new Configuration();
            if (File.Exists(dataFileName))
            {
                csvConfiguration.HasHeaderRecord = false;
            }
            using (var fileStream = new FileStream(dataFileName, FileMode.Append))
            using (var writer = new StreamWriter(fileStream))
            using (var csv = new CsvWriter(writer, csvConfiguration))
            {
                csv.WriteRecords(new[] { results });
            }
        }
    }
}
