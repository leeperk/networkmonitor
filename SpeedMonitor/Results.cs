﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpeedMonitor
{
    public class Results
    {
        public DateTime TimeStamp { get; }
        public string ServerName { get; }
        public string Isp { get; }
        public decimal Latency { get; }
        public decimal Jitter { get; }
        public decimal DownloadMbps { get; }
        public decimal DownloadBytesMB { get; }
        public decimal UploadMbps { get; }
        public decimal UploadBytesMB { get; }
        [Ignore]
        public List<string> Output { get; }
        [Ignore]
        public List<string> Error { get; }

        public Results(DateTime timeStamp, string serverName, string isp, decimal latency, decimal jitter, decimal downloadMbps, decimal downloadBytesMB, decimal uploadMbps, decimal uploadBytesMB, List<string> output, List<string> error)
        {
            TimeStamp = timeStamp;
            ServerName = serverName;
            Isp = isp;
            Latency = latency;
            Jitter = jitter;
            DownloadMbps = downloadMbps;
            DownloadBytesMB = downloadBytesMB;
            UploadMbps = uploadMbps;
            UploadBytesMB = uploadBytesMB;
            Output = output;
            Error = error;
        }
    }
}
