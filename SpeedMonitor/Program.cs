﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SpeedMonitor
{
    public class Program
    {
        private static readonly string _rootDirectory = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        private static readonly string _speedtestExecutable = Path.Combine(_rootDirectory, "speedtest.exe");
        private static readonly string _dataFileName = Path.Combine(_rootDirectory, "speedtest.csv");

        static void Main(string[] args)
        {
            var arguments = "--progress=no";
            Console.WriteLine(_speedtestExecutable);
            try
            {
                var startInfo = new ProcessStartInfo()
                {
                    FileName = _speedtestExecutable,
                    Arguments = arguments,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false
                };
                var outputs = new List<string>();
                var errors = new List<string>();

                var timestamp = DateTime.UtcNow;
                var process = Process.Start(startInfo);
                process.OutputDataReceived += (sender, e) => { outputs.Add(e.Data); Console.WriteLine(e.Data); };
                process.ErrorDataReceived += (sender, e) => { errors.Add(e.Data); Console.WriteLine(e.Data); };

                if (process.Start())
                {
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    process.WaitForExit(120000);
                    var results = GenerateResults(timestamp, outputs, errors);

                    WriteResultsToDataFile(results);
                }
            }
            catch (Exception ex)
            {
                WriteResultsToDataFile(new Results(DateTime.UtcNow, ex.Message, "", -1, -1, -1, -1, -1, -1, null, null));
            }
        }

        public static Results GenerateResults(DateTime timestamp, List<string> outputs, List<string> errors)
        {
            string serverName = "";
            string isp = "";
            decimal latency = 0;
            decimal jitter = 0;
            decimal downloadMbps = 0;
            decimal downloadBytesMB = 0;
            decimal uploadMbps = 0;
            decimal uploadBytesMB = 0;

            outputs
                .Where(e => false == string.IsNullOrWhiteSpace(e))
                .ToList()
                .ForEach(line =>
                {
                    var colonIndex = line.IndexOf(':');
                    if (colonIndex > -1)
                    {
                        var key = line.Substring(0, colonIndex).Trim().ToLowerInvariant();
                        var data = line.Substring(colonIndex + 1).Trim().ToLowerInvariant();
                        switch(key)
                        {
                            case "server":
                                serverName = data;
                                break;
                            case "isp":
                                isp = data;
                                break;
                            case "latency":
                                var parts = data.Split("(");
                                latency = Convert.ToDecimal(parts[0].Split(" ")[0]);
                                jitter = Convert.ToDecimal(parts[1].Split(" ")[0]);
                                break;
                            case "download":
                                parts = data.Split(": ");
                                downloadMbps = Convert.ToDecimal(parts[0].Split(" ")[0]);
                                downloadBytesMB = Convert.ToDecimal(parts[1].Split(" ")[0]);
                                break;
                            case "upload":
                                parts = data.Split(": ");
                                uploadMbps = Convert.ToDecimal(parts[0].Split(" ")[0]);
                                uploadBytesMB = Convert.ToDecimal(parts[1].Split(" ")[0]);
                                break;
                        }
                    }
                });
            return new Results(
                timestamp,
                serverName,
                isp,
                latency,
                jitter,
                downloadMbps,
                downloadBytesMB,
                uploadMbps,
                uploadBytesMB,
                outputs.Where(o => o != null).ToList(),
                errors.Where(e => e != null).ToList()
            );
        }

        private static void WriteResultsToDataFile(Results results)
        {
            var csvConfiguration = new Configuration();
            if (File.Exists(_dataFileName))
            {
                csvConfiguration.HasHeaderRecord = false;
            }
            using (var fileStream = new FileStream(_dataFileName, FileMode.Append))
            using (var writer = new StreamWriter(fileStream))
            using (var csv = new CsvWriter(writer, csvConfiguration))
            {
                csv.WriteRecords(new[] { results });
            }
        }
    }
}
